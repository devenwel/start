.. _readme:

Introduction
============

Working with our zaaksysteem requires working in a lot of different libraries / packages / services.
This repository acts as a kind of "mono-repo", which makes working on different services a lot easyer
by providing a lot of helper utils.

The main advantage is our use of myrepos: using the .mrconfig file we can add new repositories to this
repository, from where we can start our development on the different package.

Getting started
---------------

TL;DR;
^^^^^^

::

    git clone git@gitlab.com:zaaksysteem/start.git
    cd start

    bin/zs myrepos install
    mr --jobs=5 update               # --jobs=5 means: run 5 parallel jobs
    bin/zs zaaksysteem setup
    docker-compose up -d 

    # Python

The Zaaksysteem development environment is now available at https://dev.zaaksysteem.nl/

Long Version
^^^^^^^^^^^^

First initialize the repository by running the following command. This will ask for your gitlab
username, so we can use it later for creating forks etc.

::

    bin/zs myrepos install

After succesfull initialization, let's start by retrieving every repository by running the following
command. By using the ``--jobs=`` option, we speed it up by cloning 5 repositories in parallel.

::

    mr --jobs=5 update

Alright. Now we have our repository, let's make sure we can spin up our zaaksysteem, by setting it up.

::

    bin/zs zaaksysteem setup

So. That's it. Now start our zaaksysteem, by running ``docker-compose``, like this:

::

    docker-compose up -d 

This could take a while. Find yourself some coffee, and when docker tells you everything is ok,
wait two minutes more, and browse to ``dev.zaaksysteem.nl`` for your first encounter.

Login with ``admin/admin``

Typical Development
===================

Below are examples to start development on our systems, depending on the method, are here the
options.

Python
------

For python development, you are probably working on a http daemon or rabbitmq consumer. Typical
development involves the daemons, the domain (business logic) and the database (infra). For the http
daemon of the domain "case management", you would work in:

- zsnl-http-cm
- zsnl-domains
- zsnl-database

Using Docker
^^^^^^^^^^^^

Using docker, you would like to generate a requirements file containing links to the other libraries
in this repository. You can accomplish this by running some python utils. For instance, if you would
like to work on the zsnl-http-cm http engine, use:

::

    cd zsnl-http-cm
    mr zs python generate
    mr zs python dinstall

These two commands will generate requirements files containing links to the other directories, so
you do not have to wait for the domain repository to be pushed before you can work on the http layer
for instance.

A quick way to generate the requirements for every python package, simply run the same command in the
main "start" directory. You can also do this with ``vinstall`` and ``dinstall``. See
``mr zs python`` for more information.

::

    mr zs python generate


Using Virtualenv
^^^^^^^^^^^^^^^^

I'd like to use Visual Studio Code and all the helper utils for testing. This is why I like to work
with virtual environments. You can start using a virtual environment, by setting it up like this.
Again, we use the zsnl-http-cm as an example.

::

    cd zsnl-http-cm
    mr zs python setup
    source .venv/zsnl-http-cm/bin/activate

This will create a virtual environment in ``.venv/zsnl-http-cm`` and activates it in your terminal,
but this does not yet installs the requirements. So please run the following command for that:

::

    cd zsnl-http-cm
    mr zs python vinstall

This will run ``pip install -r REQUIREMENTSFILE`` for this repository.

You can speed all of the above up by using ``mr`` to do it for every repository. That looks like
this:

::

    mr zs python generate
    mr zs python setup
    mr zs python vinstall

This will run it for every repository containing a setup.py (python detected) file.

Perl
----

Perl is the most straightforward way for developing. Just edit some files in ``zsnl-perl-api``
and run the necessary docker-compose commands to reload the engine.

::
    
    vim zsnl-perl-api/lib/Zaaksysteem/Constants.pm

    docker-compose stop perl-api
    docker-compose up -d perl-api

JavaScript
----------

TODO

How does it fit together
========================

Todo, but let's start with this diagram:

.. image:: assets/repositories_zoom_70percent.png

Admin Interfaces
================

Some of the services used by Zaaksysteem come with web-based admin tools. These are available at
the following URLs:

* `RabbitMQ https://rabbitmq.dev.zaaksysteem.nl/` (message queue; use guest / guest to log in)
* `Minio https://rabbitmq.dev.zaaksysteem.nl/` (S3-compatible file store; see etc/minty_config.conf for login keys
* `Redis https://redis.dev.zaaksysteem.nl/` (temporary storage for session data)
* `Mailhog https://mailhog.dev.zaaksysteem.nl/` (development web-mail server)

Settings
========

Editorsettings are, when possible, described in the editorconfig. Here are some advised settings
for some common editors

Visual Studio Code
------------------

Packages
^^^^^^^^

Use ``CTRL+SHIFT+P`` or ``CMD+SHIFT+P`` (OSX) and type ``install extensions`` to install the following
extensions:

* ms-python: a fine set of extensions regarding python development
* gitlens: gives you fine grained history about the line of code you are working on

Settings
^^^^^^^^

Doing nothing here will drive you crazy, because we do require tools as ``isort``, ``black`` and
``flake8`` to be run. Go to your settings by clicking ``CTRL+SHIFT+P`` or ``CMD+SHIFT+P`` (OSX)
and type ``workspace settings`` (or ``user settings`` if you want to work permanently like this)

Or you could just create a folder called .vscode in the top "start" folder, and create
``.vscode/settings.json`` containing the following settings:

Be sure to check the ``isort`` path!

::

    {
        "python.pythonPath": ".venv/zsnl-domains/bin/python",
        "python.linting.flake8Enabled": true,
        "python.formatting.provider": "black",
        "python.formatting.blackArgs": [
            "-l",
            "79",
            "--exclude",
            "\\.eggs"
        ],
        "editor.formatOnSave": true,
        "python.sortImports.path": "/usr/local/bin/isort",
        "python.sortImports.args": [
            "-sg .eggs",
            "-m 3",
            "-tc",
            "-rc"
        ],
        "[python]": {
            "editor.codeActionsOnSave": {
                "source.organizeImports": true
            }
        },
        "editor.rulers": [
            79
        ]
    }

This will do the following:

* Configure black
* Configure isort
* Configure flake8
* Set the python interpreter to the zsnl_domains python engine (bit of hack, ok for now)
* Set Black as the default formatting provider
* Makes sure that everytime you save your file, both ``black`` as ``isort`` clean up your code
* Will hint possible ``flake8`` errors during typing

Contributing
============

Please read `CONTRIBUTING.md <https://gitlab.com/minty-python/zsnl_domains/blob/master/CONTRIBUTING.md>`_
for details on our code of conduct, and the process for submitting pull requests to us.

License
=======

Copyright (c) 2020, Minty Team and all persons listed in
`CONTRIBUTORS <https://gitlab.com/minty-python/zsnl_domains-cqs/blob/master/CONTRIBUTORS>`_

This project is licensed under the EUPL, v1.2. See the
`EUPL-1.2.txt <https://gitlab.com/minty-python/zsnl_domains/blob/master/LICENSE>`_
file for details.
