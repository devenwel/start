***Getting Started with the development process / Gitlab / Database Changes / tips / etc***

Ensure you have the following in your ~/.gitconfig

	$ cat ~/.gitconfig 
	[...]
	[merge]
		ff = only

This avoids unnessecary problems when using git pull/push , you can override 
with --no-ff, but ask yourself: "do I really need to?"

***I - When starting new development:***

Ensure you have latest master:

	git checkout master
	git pull
	git checkout -b <mintlab-username>/MINTY-<NR>-<bugfix/feat>-<description>

	example branche name: robin/MINTY-3835-feat-create_pdf_document_from_pip_message

***II - When continuing development in feature branch:***

1)	Start of day to ensure up to date code:
 	in feature branch:

      	git rebase -i origin master
        ( see https://about.gitlab.com/blog/2019/02/07/start-using-git/ for cleaning up commit messages)

2) 	[code... code... code...]

3)  	Make sure that **isort**, **black** and **flake** have run on your code 
        especially when your editor does not do this automatically!
	isort, black and flake should be in your virtualenv when using start.git

	- isort 	( alias is='isort -sg .eggs -sg .env -m 3 -tc -rc -ds -y' )
	- black 	( alias bl='black -l 79 --exclude \\.eggs\|\\.env\|\\.git .' )
	- flake8 	( alias fl='flake8 --exclude=.svn,CVS,.bzr,.hg,.git,__pycache__,.tox,.eggs,*.egg,.env' )

It is advisable to add ad git pre-commit hook for these command so you never accidentally send un-isorted, un-blacked and/or unflake'ed code into branches.

4)	Add to git:

	git add .
	
	git commit -m "Good commit message"
	
	git push

5) 	Check pipeline on gitlab for possible errors. 


***III - Testing***

 In the development process you've also created tests (right?). 
 To make sure your tests run without failure and your code did not 
 mess up existing code: 

1)	Make sure you are in the correct virtual environment for your code.

2a)	Run all the tests with verbose output:

	pytest -svvx
	
	(-vv extra verbose , -s shows print("output") in tests,-x stop on first test failure. )

2b)	When all available tests are correct and you only want to run
	tests regarding your own code/tests you can give a path for
	test collection, eg:

	pytest -vv ./tests/document/test_document_commands.py 

3)	Make sure you have covered all possible code and code paths.

	pytest -vv -cov --cov-report=html

The output will be in: ~/start/zsnl-whatever/htmlcov/index.html

Running with coverage report takes significantly longer especially
for large codebases like zsnl_domains. So when all code is covered
and only existing tests fail you maybe don't want to use this every time
you run tests.

***IV - Merging to development***

1) 	When in feature branch: 

	git checkout development

When there are still any changes to be commited in your feature branch
take a look at them. There shouldn't be any after following the above steps for
developing. So either throw them out (git checkout changed-file), stash them 
(git stash) for possible later use or reference, or commit them following
steps 3 to 6 in section II

	git pull --ff-only  (ff = only should already be in your ~/.gitconfig)

If there are any errors: 

	git reset --hard origin/development

note: this throws away any local changes in the development branch,  there shouldn't usually be any anyway.

	git merge --no-ff <feature branch>
	git push origin development
	
Check pipeline on CI/CD gitlab, should be ok since your work in the 
feature branch was up to date when you followed steps in section II
   

***V - Creating a merge request***

1) 	In Gitlab create Merge Request. 


When creating a merge request make sure you have a correct MR title, eg:
	
	"MINTY-3411: As an asignee i can save e-mail to Documents as PDF"

If you are still developing on the new feaure ensure tyou have an 
"WIP:" in the name so  it can't be merged accidentally to master.

	"WIP: MINTY-3411: As an asignee i can save e-mail to Documents as PDF"


2)    "Delete branch after merge = true"

 - If tester gives an OK for new features:

3) 	remove the "WIP:" from the merge request title.

4) 	Ask review on Slack @development / @python / or any other means

5a)  Review comments given by the reviewer, and adjust code accordingly if needed.

5b)	Commit any possible changes (see above)

6) 	make sure you get an approval/thumb up from reviewer(s).

7) 	merge to master on Gitlab.

***VI - If there are merge conflicts according to Gitlab on merge to master:***

When Gitlab complains that there are merge conflicts regarding merge to master, 
you can look at the merge conflicts on the Gitlab page, but fix them locally: 

• In feature branch:

	git fetch
	git rebase -i origin/master
 
 This makes the changes in master become the new basis of your work in your feature branch. 
 
-i (interactive) gives you the ability to clean up your commit messages in your feature branch.
see [https://about.gitlab.com/blog/2019/02/07/start-using-git/](https://about.gitlab.com/blog/2019/02/07/start-using-git/) 
for cleaning up commit your messages

Follow the git cli guidelines about resolving, adding and continuing the rebase until done, this will result in a:

	"Successfully rebased and updated refs/heads/<YOUR-FEATURE-BRANCH>"

message from the git CLI. After that you can force push your feature branch: 

      git push -f origin <feature-branch>. 


***VII - Resetting development to master***

In master:

	git fetch
	git checkout development
	git reset --hard origin/development
	
When not only local (this would mostly be in the beginning of the sprint)

	git push -f origin development


***IIX - Making database changes*** 

• In Perl-repo (zsnl-per-api) make sure you have latest master
• Create feature branch of master
• in **db/upgrade/NEXT/** create a file: **xxxx-description.sql ** 
    xxxx is a followup number start with 1000 if nothing present and work up from 
    there, does not need to be +1 which could interfere with possible other db-change 
    commits in the sprint, also named xxxx + 1,  but at least a different higher number. 
	
• Fill the .sql file with the database changes. Make sure to use transactions eg:

	BEGIN;
	    ALTER TABLE x ADD COLUMN y .... ;
	COMMIT;

• Run the script in your local docker container: 

	docker-compose exec database psql -U zaaksysteem
there give the command:

	\i /opt/zaaksysteem/db/upgrade/NEXT/etc

• If that runs without errors, then outside of the container but in the perl-api repo dir: 

        COMPOSE_FILE=../docker-compose.yml ./dev-bin/update_database.sh ./db/upgrade/NEXT/1000-your-script.sql

• The result should be a change on "template.sql", "test-tenplate.sql" and some perl database files.

• Make the correct changes in zsnl-database eg. the schema files and maybe others depeding on the changes you made
       (follow the usual development process for changes in zsnl-database, feature-branch, MR, etc)

• Make sure that the databasechanges in the .sql file are correct, and make sure all stakeholders 
in this change/interrested parties are up-to-date about the database-change. 

• Ask ops to roll .sql file out on development databases.


**Rollout on master**

• merge zsnl-perl-api (parallel to this: ask ops to roll out the update script wherever db is running on master code)
• merge zsnl-database with the changes to the schema files etc to master
• if everything went ok, the zsnl-database should probably have a new version number. See IX changing version tags.


**Modifications to your own changes**

If by any chance a database change you made alters again within the sprint, but after a while (when things have been
rolled out already), always add an extra alteration .sql file with an increased version number and a database alteration 
on your changes. For example a rename of a fields you created. So that database upgrades can always be done in sequence. 

Eg: you and others should always be able to do: 

    for i in db/upgrade/NEXT/*.sql; do
    echo docker-compose exec database psql -U zaaksysteem zaaksysteem -f /opt/zaaksysteem/$i
    done
    
Without errors and end up with a correct database.

Also because all 'interrested parties'/stakeholders as mentioned above can in the meantime be a lot 
of different (non-)teammembers  above could have people end up locally with different database, 
depending wether or not they have applied your initial changes already on their local datbase. 

And _if_ unneeded .sql files are left over also git-rm them from development, since people may have pulled from 
development in the meantime, and are left with unneeded .sql files in their repo, which makes things less clear.



***IX - Changing version tags for zsnl modules***

When MR's are ok and merged into master it is probable/possible that other modules need to depend on a newer 
version of said module

• Merge MR to master following above process (V and/or in case of database changes IIX)

	git checkout master
	git pull --ff-only
	bumpversion patch   ( probably patch, version numbers use semantic versioning, eg: major.minor.patch notation) 
	git push origin v<new-version-number>

After this the new version can be added in your requirements.txt files for other modules.

Note I : when pushing to development and you get merge conflicts regarding requirements.txt, 
take into account that requirements.txt in the development branch can always depend on 
development versions of other modules. 

Note II : requirements.txt in the master branch should, offcourse, never depend on development
versions.


***X - Some usefull aliases, functions, terminal/CLI enhancements etc.***

• To show your current git branch in terminal and other utils:
	https://github.com/magicmonty/bash-git-prompt.git 

• In Linux terminal set the terminal title, put in your ~/.bashrc:

	# set title of current terminal 
	setTerminalTitle(){
	       echo -ne "\033]0;${1}\007" 	
	}

• git diff with human readable line numbers. Put in your ~/.bashrc

	gdiff() {
		git diff --color=always $1 | \
		gawk '{bare=$0;gsub("\033[[][0-9]*m","",bare)};\
	    	match(bare,"^@@ -([0-9]+),[0-9]+ [+]([0-9]+),[0-9]+ @@",a){left=a[1];right=a[2];next};\
		bare ~ /^(---|\+\+\+|[^-+ ])/{print;next};\
		{line=gensub("^(\033[[][0-9]*m)?(.)","\\2\\1",1,$0)};\
		bare~/^-/{print "-"left++ ":" line;next};\
		bare~/^[+]/{print "+"right++ ":" line;next};\
		{print "("left++","right++"):"line;next}'
	}

• In your ~/.bash_aliases pick and choose: 

	alias is='isort -sg .eggs -sg .env -m 3 -tc -rc -ds -y'
	alias bl='black -l 79 --exclude \\.eggs\|\\.env\|\\.git .'
	alias fl='flake8 --exclude=.svn,CVS,.bzr,.hg,.git,__pycache__,.tox,.eggs,*.egg,.env'
	alias ga='git add '
	alias gc='git commit -m '
	alias gd='git diff'
	alias grep='grep --color=auto'
	alias gs='git status'
	alias dc='docker-compose'
	alias zsdb='COMPOSE_FILE=~/your_start.git_base_dir/docker_compose.yml docker-compose exec database psql -U zaaksysteem'



