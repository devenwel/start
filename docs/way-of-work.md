# Way of work

When coding on our different projects, there is a certain etiquette we bound
to. It makes things clear for everyone. To keep it short, below a listing and
examples of our way of working

## In family we trust

There is a lot of trust in each other when coding on our projects. We believe
we selected our team mates based on a culture in which they always thrive to 
build the best solution possible. We do not need to build checks to see if a
teammate made any shortcuts he/she should not do. And even when this happens,
we trust in a system of feedback to prevent this from happening in the future.

We will see what we mean by this in the process below.

## Our process

We follow a process in which we believe makes the best software. This document
aims to give technical information, so we assume every story and bug we are
talking about below is already refined and clear enough to work with.

1. **Technical plan**

    Before we start coding, we make sure we have a technical understanding of
    the feature and bug, by taking the time to discuss any thought of solutions
    with my coworker. This prevents me from writing solutions which may already
    exist somewhere in the codebase.

2. **Coding**

    We use our Start repository when coding, so when I talk about related
    repositories or locations of my code, everyone is on the same page.

    We start by making a branch, which uses the format:

        [gitlabusername]/MINTY-[NR]-[feat/bugfix]-description
    
    example:

        git checkout -b michielootjers/MINTY-1234-feat-add_document_to_a_case

    Depending on the type of bug or feature, choose the correct branch. For
    bugfixes on production, make sure you branch of `production`, but in any
    other situation you better branch of `master`.

3. **Testing**

    Whenever you feel confident enough that your code is ready, please feel
    free to send it to our development branch. It automatically deploys to
    our https://development.zaaksysteem.nl environment.

    Example:

        git checkout development
        git pull
        git merge michielootjers/MINTY-1234-feat-add_document_to_a_case
        git push

    We move our MINTY-1234 ticket in Jira to the next lane, our Test lane, so
    our test team gets informed and tests the changes and acceptance criteria
    in your JIRA ticket.

4. **To Merge**

    Assuming everything went well, it is time to bring your ticket to the
    branch you started with (either `production` or `master` from the previous
    examples).

    Make sure your branch is clean, and preferably are all changes squashed
    in one commit. After that, please make a public merge request to the chosen
    branch and ask for a code review.

    When a coworker starts reviewing your code, they will point out all the
    hints and tips they can give you to improve your code. It is up to you to
    learn from it and to improve your code according to this feedback. The
    reviewer will only "showstop" your code review when he/she sees an obvious
    bug or security incident or something which will really break the system.
    In any other case the reviewer will accept your merge request by :thumbsup:
    your review. This way we also comply to the 4-eyes principle in most formal
    audits this system goes through.